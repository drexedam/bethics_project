
# This is the user-interface definition of a Shiny web application.
# You can find out more about building applications with Shiny here:
#
# http://shiny.rstudio.com
#

library(shiny)

shinyUI(fluidPage(

  # Application title
  titlePanel("Comparison of Gov. Debt and Money Supply"),

  # Sidebar with a slider input for number of bins
  sidebarLayout(
    sidebarPanel(
      selectInput("country", "Choose a country:",
                  choices = c("Austria", "Germany", 
                              "United States", "Nigeria", "China")),
      sliderInput("range", "Year Range:", 
                  min=1960, max=2013, value=c(1960,2013), sep = "")
    ),

    # Show a plot of the generated distribution
    mainPanel(
      tabsetPanel (
        tabPanel(titel = "M2 (USD)", value = "PANEL_SUM1", h4("M2 Summary (cur. USD)"), verbatimTextOutput("summary1")),
        tabPanel(titel = "Gov. Debt (USD)", value = "PANEL_SUM2", h4("Gov. Dept Summary (cur. USD)"), verbatimTextOutput("summary2")),
        tabPanel(titel = "Correlation (USD)", value = "PANEL_COREL1", h4("Correlation (cur. USD)"), verbatimTextOutput("corel1")),
        tabPanel(titel = "M2 (Perc)", value = "PANEL_SUM3", h4("M2 Summary (percentage)"), verbatimTextOutput("summary3")),
        tabPanel(titel = "Gov. Debt (Perc)", value = "PANEL_SUM4", h4("Gov. Dept Summary (percentage)"), verbatimTextOutput("summary4")),
        tabPanel(titel = "Correlation (Perc)", value = "PANEL_COREL2", h4("Correlation (percentage)"), verbatimTextOutput("corel2"))
      ),
#      tableOutput("distPlot")
      tabsetPanel (
        tabPanel(titel = "M2 and Gov. Debt (cur. USD)", value = "PANEL_PLOT1", h4("M2 and Gov. Debt (cur. USD)"), plotOutput("distPlot")),
        tabPanel(titel = "M2 and Gov. Debt (percentage)", value = "PANEL_PLOT1", h4("M2 and Gov. (percentage)"), plotOutput("percPlot"))
      )
      
    )
  )
))
